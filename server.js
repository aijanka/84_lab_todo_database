const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

const config = require('./config');
const users = require('./app/users');
const tasks = require('./app/tasks');

const app = express();
app.use(cors());
app.use(express.json());

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once("open", () => {
    app.use('/users', users());
    app.use('/tasks', tasks());

    app.listen(8000);
})