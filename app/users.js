const express = require('express');
const nanoid = require('nanoid');
const User =require('../models/User');

const createRouter = () => {
    const router = express.Router();

    router.post('/', (req, res) => {
        const u = req.body;
        u.token = nanoid();
        const user = new User(u);

        user.save()
            .then(user => res.send(user))
            .catch(error => res.status(400).send(error));
    });

    router.post('/sessions', async (req, res) => {
        const user = await User.findOne({name: req.body.name});

        if(!user) return res.status(404).send('Username not found!');

        const isMatch = await user.checkPassword(req.body.password);

        if(!isMatch) return res.status(400).send({error: 'Password is wrong'});

        // user.token = nanoid(9);
        await user.save();
        res.send(user);
    });

    return router;
};

module.exports = createRouter;