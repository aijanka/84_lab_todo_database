const express = require('express');
const auth = require('../middlewares/auth');
const Task = require('../models/Task');

const createRouter = () => {
    const router = express.Router();

    router.post('/', auth, (req, res) => {
        const task = new Task();
        task.set({
            user: req.user._id,
            title: req.body.title,
            description: req.body.description,
            status: 'new'
        });
        task.save()
            .then(task => res.send(task))
            .catch(error => res.sendStatus(404));
    });

    router.get('/',auth, (req, res) => {
        Task.find({user: req.user._id})
            .then(tasks => res.send(tasks))
            .catch(error => res.status(404).send(error));
    });

    router.put('/:id',auth, (req, res) => {
        Task.findOne({_id: req.params.id}).populate('User')
            .then(task => {
                if(!(task.user.equals(req.user))) return res.status(401).send("Not your task!");
                const changed = req.body;
                task.set({
                    user: req.user._id,
                    title: changed.title,
                    description: changed.description,
                    status: changed.status
                })
                task.save()
                    .then(task => res.send(task))
                    .catch((error) => res.status(404).send(error));
            });

    });

    router.delete('/:id', auth, (req, res) => {
        const task = new Task();
        Task.findOne({_id: req.params.id}).populate('User')
            .then(task => {
                console.log(task, 'task')
                if(!(task.user.equals(req.user._id))) res.status(401).send("Not your task!");
                else {
                    task.remove();
                    res.send({"message": "Task is deleted"});
                }
            })
    });

    return router;
};

module.exports = createRouter;